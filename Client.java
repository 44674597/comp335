import java.io.*;
import java.net.Socket;
import java.net.UnknownHostException;
import java.util.ArrayList;

public class Client 
{	
	//SERVER CONFIG
	private static final int PORT = 8096;
	private static final String SERVERIP = "127.0.0.1";
		
	private Socket serverHandle = null;

	private BufferedReader ServerMsgHandle = null;	
	private DataOutputStream clientOutputMsgHndle = null;
	
	ArrayList<ServerInfo> servers = new ArrayList<ServerInfo>();
	ArrayList<String> serverTypes = new ArrayList<String>();
			
	boolean scheduledAllJobs = false;
	
	boolean getServersFromServerOnce = false;

	//Represents which scheduling algorithm to use
	//0=allToLargest, 1=First-fit, 2=Best-fit, 3=Worst-fit
	int algorithm = 0; 
	
	public Client(String[] args)
	{
		SetupServerAndClientHandles();
		
		//If we get here we have our input and output streams and a server handle successfully.

		//Initialize which scheduling algorithm to use.
		//Defaults to 0 which is Stage 1 allToLargest algorithm
		
		if(args.length >= 2 && args[0] == "-a")
		{
			if(args[1] == "ff")
				algorithm = 1;
			if(args[1] ==  "bf")
				algorithm = 2;
			if(args[1] == "wf")
				algorithm = 3;
		}
		
		//Initialize the back and forth with the server,		
		boolean succeeded = InitialiseServerContact();
		
//		if successful start scheduling jobs.
		if(succeeded)
		{	
			//while there are still jobs to schedule
			while(!scheduledAllJobs)
			{
				if(algorithm == 1)
					FirstFit();
				else if (algorithm == 2)
					BestFit();
					
				else if (algorithm == 3)
					WorstFit();
				//Default scheduling algorithm from stage 1.
				else GetAndScheduleJob();
			}			
		}
		
		//With no more jobs, send close messages to the server and close down.
		Cleanup();		
	}
	
	void SetupServerAndClientHandles()
	{
		try {
			serverHandle = new Socket(SERVERIP, PORT);
		} 
		catch (UnknownHostException e) 
		{
			System.err.println("COULD NOT CONNECT TO SERVER: ");
			e.printStackTrace();
		} 
		catch (IOException e) 
		{
			System.err.println("COULD NOT CONNECT TO SERVER: ");
			e.printStackTrace();
		}
				
		//If we get here we have a successful serverHandle;
						
        try {
        	ServerMsgHandle = new BufferedReader(new InputStreamReader(serverHandle.getInputStream()));
		} catch (IOException e2) {
			System.err.println("Failed to initialise serverInput");
			e2.printStackTrace();
		}
 
		try {
			clientOutputMsgHndle = new DataOutputStream(serverHandle.getOutputStream());
		} catch (IOException e1) {
			System.err.println("trouble initialising the client output stream");
			e1.printStackTrace();
		}
	}

	void FirstFit()
	{
	    //Get current job info
		Job job = GetNextJob();

		//End if there are no more jobs
		if(job == null)
		    return;

        //Get the current server state info from ds-server
		SendMessage("RESC Avail " + job.getCpuCores() + " " + job.getMemory() + " " + job.getDisk());
		String serverInfoStart = ReceiveMessage();
		if(!serverInfoStart.equals("DATA"))
			System.err.println("Wrong Server Reply, expecting DATA");

        //Parse server state info
		getServersFromServerOnce = true;
		GetAllServers();

		//Find the smallest server(CPU) size that fits the current job
		int curCpuCores =  findSmallestServerSize(-1);

		for(ServerInfo server: servers) //For each server
		{
			if(server.getCpuCores() == curCpuCores) //If the server size matches the smallest size needed for the current job
			{
				//Schedule the job
				SendMessage("SCHD " + job.getJobID() + " " + server.getServerType() + " " + server.getiD());

				//Check that ds-server accepts the scheduling
                if(!ReceiveMessage().equals("OK"))
                {
                    System.err.println("Job was not scheduled correctly, expected reply 'OK'");
                }

                //End loop and method, current job has been scheduled.
				return;

			}
		}
	}

	//Note(Jack): This function will retrieve the next job from the server and place it into a Job object.
	Job GetNextJob()
	{
		Job result = null;
		
		//Get First Job
		SendMessage("REDY");
		String jobMsg = ReceiveMessage();				
		if(jobMsg.equals("NONE"))
		{
			scheduledAllJobs = true;
			return result;
		}
		
		result = new Job(jobMsg);
		return result;
	}
	
	//Note(Jack): This calculates the fitness value, serverCpuCores - jobCpuCores.
	int CalculateFitnessValue(int serverCores, int jobCores)
	{
		return Math.abs(serverCores - jobCores);
	}	

	//Note(jack): This function determines if the server has sufficient resources.
	//it is assumed that the server has sufficient resources if it is inactive, active or idle as per the general discussions. 
	boolean ServerHasSufficientRes(ServerInfo server, Job job)
	{
		if(server.getServerState() == 0 || server.getServerState() == 2 || server.getServerState() == 3)
		{
			if(job.getCpuCores() <= server.getCpuCores() && job.getMemory() <= server.getMemory() && job.getDisk() <= server.getDisk())
			{
				return true;
			}
			else
			{
				return false;
			}						
		}
		else
		{
			return false;
		}		
	}
	
	//Note(Jack): This function is for the best-active inital resource capacity, it uses the initial server resources from
	//the system.xml. 
	boolean ServerHasSufficientRes(ServerInfo server, int cpuCores, int disk, int memory, Job job)
	{
		//If our server is idle, inactive or active
		if(server.getServerState() == 0 || server.getServerState() == 2 || server.getServerState() == 3)
		{
			//if the server has the available resources.
			if(job.getCpuCores() <= cpuCores && job.getMemory() <= memory && job.getDisk() <= disk)
			{
				return true;
			}
			else
			{
				return false;
			}						
		}
		else
		{
			return false;
		}		
	}
			
	void BestFit()
	{
		//Retrieve our next job.
		Job job = GetNextJob();
				
		//If we dont have a job the server has returned 'NONE' so return.
		if(job == null)
		{
			return;
		}
				
		int bestFit = Integer.MAX_VALUE;
		int minAvail = Integer.MAX_VALUE;				

		//This is the current best server.
		ServerInfo savedServer = null;
			
		//ParsedXML is a helper class which will read system xml and 
		//put the server data into helpful collections.
		ParsedXML pX = new ParsedXML();
		pX.ParseXMLFile("system.xml");
				
		//For every server type found in the system.xml (in order)
		for(String serverType : pX.serverTypes)
		{
			//Get all servers of type serverType
			SendMessage("RESC Type " + serverType);
			String serverInfoStart = ReceiveMessage();
			if(!serverInfoStart.equals("DATA"))
			{
				System.err.println("Wrong Server Reply, expecting DATA");
			}
			
			//Note(Jack): If this is true it will clear the server list.
			getServersFromServerOnce = true;
			//This will get all the servers with the correct server type.
			GetAllServers();
				
			//This is just a quick way to get the limit of the server type.
			int limit = pX.GetServersOfType(serverType).get(0).getLimit();
								
			//For i = 0 to num of servers or while i is less than the limit - 1.
			for(int i = 0; i < servers.size() && (i < limit - 1); i++)
			{
				//Retrieve our server from the list.
				ServerInfo server = servers.get(i);
				
				//If our server has sufficient resources.
				if(ServerHasSufficientRes(server, job))
				{
					//Calculate Fitness Value
					int fitnessValue = CalculateFitnessValue(server.getCpuCores(), job.getCpuCores());
					
					//If our fitness value is better than the previous found or they are equal and the available server time is better than the previous.
					if(fitnessValue < bestFit || ((fitnessValue == bestFit) && (server.getAvailableTime() < minAvail)))
					{						
						bestFit = fitnessValue;
						minAvail = server.getAvailableTime();
						savedServer = server;
					}
				}
			}		
		}
		
		//if we have found a best fit.
		if(bestFit != Integer.MAX_VALUE)
		{		
			//Schedule the job.
			SendMessage("SCHD " + job.getJobID() + " " + savedServer.getServerType() + " " + savedServer.getiD());
			if(!ReceiveMessage().equals("OK"))
			{
				System.err.println("Job was not scheduled correctly, expected reply 'OK'");
			}
			return;
		}
		else
		{				
			//This is the best-fit active server based on initial resource capacity, we are using the initial
			//resource capacity found in the system.xml, and are assuming that the server states acceptable are 0, 2 and 3.
			//Inactive, Idle or Active.
			
			bestFit = Integer.MAX_VALUE;
			minAvail = Integer.MAX_VALUE;				
			savedServer = null;
			
			for(String serverType : pX.serverTypes)
			{
				//Get all servers of type serverType
				SendMessage("RESC Type " + serverType);
				String serverInfoStart = ReceiveMessage();
				if(!serverInfoStart.equals("DATA"))
				{
					System.err.println("Wrong Server Reply, expecting DATA");
				}			
				//Note(Jack): If this is true it will clear the server list.
				getServersFromServerOnce = true;
				GetAllServers();
						
				//This is just a quick way to get the limit of the server type.
				int limit = pX.GetServersOfType(serverType).get(0).getLimit();
				//Retrieves the initial server data.
				int initialServerCpuCores = pX.GetServersOfType(serverType).get(0).getCpuCores();
				int initialServerDisk = pX.GetServersOfType(serverType).get(0).getDisk();
				int initialServerMemory = pX.GetServersOfType(serverType).get(0).getMemory();
				
				for(int i = 0; i < servers.size() && (i < limit - 1); i++)
				{
					ServerInfo server = servers.get(i);
					
					if(ServerHasSufficientRes(server, initialServerCpuCores, initialServerDisk, initialServerMemory, job))
					{
						//Calculate Fitness Value
						int fitnessValue = CalculateFitnessValue(initialServerCpuCores, job.getCpuCores());
						
						if(fitnessValue < bestFit || (fitnessValue == bestFit && server.getAvailableTime() < minAvail))
						{
							bestFit = fitnessValue;
							minAvail = server.getAvailableTime();

							savedServer = server;
						}
					}
				}			
			}
			
			if(bestFit != Integer.MAX_VALUE)
			{		
				SendMessage("SCHD " + job.getJobID() + " " + savedServer.getServerType() + " " + savedServer.getiD());
				if(!ReceiveMessage().equals("OK"))
				{
					System.err.println("Job was not scheduled correctly, expected reply 'OK'");
				}
				return;
			}
			else
			{
				//If we have not found any suitable servers at this point, notify the client and return.
				System.err.println("Could not find a server.");
				return;
			}
		}				
	}
	
	void WorstFit() //Brodie Hamdorf 44882246
	{
		//Set worstFit, altFit, fitnessValue
		int worstFit = Integer.MIN_VALUE;
		int altFit   = Integer.MIN_VALUE;
		int fitnessValue;

		//To differentiate between the worst and the alt server
		ServerInfo worstServer = null;
		ServerInfo altServer = null;

		//get Jobs (credit to Henryk)
		Job job = GetNextJob();
		if(job == null)
		{
			return;
		}

		//Iterate through each server type in xml order (credit to Jack)
		ParsedXML pX = new ParsedXML();
		pX.ParseXMLFile("system.xml");
		for (String serverType : pX.serverTypes)
		{
			//For each server of type i, from 0 to limit (credit to Jack)
			SendMessage("RESC Type " + serverType);
			String serverInfoStart = ReceiveMessage();

			if(!serverInfoStart.equals("DATA"))
			{
				System.err.println("Wrong Server Reply, expecting DATA");
			}
			//Note(Jack): If this is true it will clear the server list.
			getServersFromServerOnce = true;
			//This will get all the servers with the correct server type.
			GetAllServers();
			
			int limit = pX.GetServersOfType(serverType).get(0).getLimit();

			for (int i = 0; (i < servers.size()) && (i < limit - 1); i++)
			{

				//Check if Server has enough resources for job
				ServerInfo server = servers.get(i);

				if (server.getCpuCores() >= job.getCpuCores())
				{
					//Calculate FitnessValue (delta of server cores and job cores needed
					fitnessValue = Math.abs((server.getCpuCores() - job.getCpuCores()));

					//Check if fitness value > worstFit and server available now, set worstFit if true
					if ((fitnessValue > worstFit) && (server.getServerState() == -1))
					{
						worstFit = fitnessValue;
						worstServer = server;
					}
					//else check if fitness value > altFit and server available soon, set altFit if true
					else if ((fitnessValue > altFit) && (server.getAvailableTime() < job.getEstimatedRunTime()))
					{
						altFit = fitnessValue;
						altServer = server;
					}
				}
			}
		}

		//Schedule jobs
		if(worstFit != Integer.MIN_VALUE)
		{
			SendMessage("SCHD " + job.getJobID() + " " + worstServer.getServerType() + " " + worstServer.getiD());
			if(!ReceiveMessage().equals("OK"))

			{
				System.err.println("Job was not scheduled correctly, expected reply 'OK'");
			}

			return;
		}
		else if (altFit != Integer.MIN_VALUE)
		{
			SendMessage("SCHD " + job.getJobID() + " " + altServer.getServerType() + " " + altServer.getiD());

			if(!ReceiveMessage().equals("OK"))
			{
				System.err.println("Job was not scheduled correctly, expected reply 'OK'");
			}

			return;

		}
		else
		{
			SendMessage("SCHD " + job.getJobID() + " " + altServer.getServerType() + " " + altServer.getiD());
			if(!ReceiveMessage().equals("OK"))
			{
				System.err.println("Job was not scheduled correctly, expected reply 'OK'");
			}

			return; //active server with worst fit based on initial resource capacity
		}

	}

	void GetAndScheduleJob()
	{
		//Get First Job
		SendMessage("REDY");
		String JobMsg = ReceiveMessage();				
		if(JobMsg.equals("NONE"))
		{
			scheduledAllJobs = true;
			return;
		}
		Job job1 = new Job(JobMsg);
		
		//Signal to the server that we want it to start sending the available server list.
		
//		SendMessage("RESC Avail " + job1.CpuCores + " " + job1.memory + " " + job1.disk);
//		SendMessage("RESC All " + job1.CpuCores + " " + job1.memory + " " + job1.disk);
		SendMessage("RESC All ");
		String serverInfoStart = ReceiveMessage();
		if(!serverInfoStart.equals("DATA"))
		{
			System.err.println("Wrong Server Reply, expecting DATA");
		}
		
		if(servers.isEmpty())
		{
			getServersFromServerOnce = true;
		}
		
		//Parse all the server information 

		GetAllServers();
		//Find the largest server the one with the most cpu cores.
		ServerInfo largest = FindLargestServer();
		if(largest == null)
		{
			System.err.println("Could not find largest server");
		}
				
		//Schedule the job to the largest server.
		SendMessage("SCHD " + job1.getJobID() + " " + largest.getServerType() + " " + largest.getiD());
		
		if(!ReceiveMessage().equals("OK"))
		{
			System.err.println("Job was not scheduled correctly, expected reply 'OK'");
		}
	}

	//Note(Jack): This finds the server with the largest amount of cpu  cores.
	ServerInfo FindLargestServer()
	{
		ServerInfo result = null;
		
		int currLargestCpuCores = Integer.MIN_VALUE;
		
		for(ServerInfo info : servers)
		{
			if(info.getCpuCores() > currLargestCpuCores)
			{
				currLargestCpuCores = info.getCpuCores();
				result = info;
			}
		}
		
		return result;
	}

	//Finds smallest serversize (cpu cores) ABOVE the given lowerbound.
	int findSmallestServerSize(int lowerBound) 
	{
		int curSmallest = Integer.MAX_VALUE;
		for(ServerInfo server : servers) 
		{
			if(server.getCpuCores() < curSmallest && server.getCpuCores() > lowerBound)
				curSmallest =  server.getCpuCores();
		}
		return curSmallest;
	}
	
	void PopulateServerInfo(ServerInfo info)
	{
		String serverInfo = ReceiveMessage();
		
		if(serverInfo.equals("."))
		{
			endOfServerList = true;
			return;
		}
		
		String[] serverArr = serverInfo.split(" ");
		
		info.setServerType(serverArr[0]);
		info.setiD(Integer.parseInt(serverArr[1]));
		info.setServerState(Integer.parseInt(serverArr[2]));
		info.setAvailableTime(Integer.parseInt(serverArr[3]));
		info.setCpuCores(Integer.parseInt(serverArr[4]));
		info.setMemory(Integer.parseInt(serverArr[5]));
		info.setDisk(Integer.parseInt(serverArr[6]));
	}
	
	boolean endOfServerList = false;	
	void GetAllServers()
	{
		if(getServersFromServerOnce)
			servers.clear();
	
		endOfServerList = false;
		SendMessage("OK");
		while(!endOfServerList)
		{
			ServerInfo info = new ServerInfo();			
			PopulateServerInfo(info);
						
			if(!endOfServerList)
			{
				if(getServersFromServerOnce)
				{
					servers.add(info);
				}
				SendMessage("OK");
			}
		}
		
		getServersFromServerOnce = false;
	}
	
	
	void Cleanup()
	{
		SendMessage("QUIT");
		if(!ReceiveMessage().equals("QUIT"))
		{
			System.err.println("Server did not respond correctly to the QUIT command");
		}
				
		try 
		{
			ServerMsgHandle.close();
			clientOutputMsgHndle.close();
			serverHandle.close();
		} 
		catch (IOException e) 
		{
			System.err.println("Trouble closing handles.");
			e.printStackTrace();
		}
	}
	
	boolean InitialiseServerContact() 
	{
		boolean result = true;
		SendMessage("HELO");
		
		if(ReceiveMessage().equals("OK"))
		{
			SendMessage("AUTH comp335");
			
			if(ReceiveMessage().equals("OK"))
			{
			}
			else
			{
				result = false;
				System.out.println("Expected server message 'OK'");
			}
			
		}
		else
		{
			result = false;
			System.out.println("Expected server message 'OK'");
		}
		
		return result;
	}
	
	void SendMessage(String msg)
	{
		msg = msg + "\n";
		try 
		{
			clientOutputMsgHndle.write(msg.getBytes());
		} 
		catch (IOException e) {
			e.printStackTrace();
		}				
	}
	
	String ReceiveMessage()
	{		
		String result = "";
		try 
		{
			result = ServerMsgHandle.readLine();
		} 
		catch (IOException e) 
		{
			e.printStackTrace();
		}
		
		return result;
	}
	
	public static void main(String[] args) 
	{
		new Client(args);
	}
}

