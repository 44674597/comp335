
public class Job
{
	public int getSubmitTime() {
		return submitTime;
	}
	
	public void setSubmitTime(int submitTime) {
		this.submitTime = submitTime;
	}
	
	public int getJobID() {
		return jobID;
	}
	
	public void setJobID(int jobID) {
		this.jobID = jobID;
	}
	
	public int getEstimatedRunTime() {
		return estimatedRunTime;
	}
	
	public void setEstimatedRunTime(int estimatedRunTime) {
		this.estimatedRunTime = estimatedRunTime;
	}
	
	public int getCpuCores() {
		return CpuCores;
	}
	
	public void setCpuCores(int cpuCores) {
		CpuCores = cpuCores;
	}
	
	public int getMemory() {
		return memory;
	}
	
	public void setMemory(int memory) {
		this.memory = memory;
	}
	
	public int getDisk() {
		return disk;
	}
	
	public void setDisk(int disk) {
		this.disk = disk;
	}
	
	private int submitTime;
	private int jobID;
	private int estimatedRunTime;
	private int CpuCores;
	private int memory;
	private int disk;
	
	public Job(String jobMsg)
	{
		String[] jobArr = jobMsg.split(" ");
		
		submitTime = Integer.parseInt(jobArr[1]);
		jobID = Integer.parseInt(jobArr[2]);
		estimatedRunTime = Integer.parseInt(jobArr[3]);
		CpuCores = Integer.parseInt(jobArr[4]);
		memory = Integer.parseInt(jobArr[5]);
		disk = Integer.parseInt(jobArr[6]);		
	}
}
