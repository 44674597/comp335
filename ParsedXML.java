import java.io.File;
import java.util.ArrayList;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

public class ParsedXML {
	
	ArrayList<ServerInfo> servers = new ArrayList<ServerInfo>();
	ArrayList<String> serverTypes = new ArrayList<String>();
	
	public ParsedXML()
	{
		
	}
	
	public ArrayList<ServerInfo> GetServersOfType(String type)
	{
		ArrayList<ServerInfo> result = new ArrayList<ServerInfo>();
		
		for(ServerInfo server : servers)
		{
			if(server.getServerType().equals(type))
			{
				result.add(server);
			}
		}
		
		return result;
	}
	
	public void ParseXMLFile(String fileLocationAndName)
	{
		try 
		{		
			File inputFile = new File(fileLocationAndName);		
			if(!inputFile.exists())
			{
				System.out.println("File does not exist");
			}
			
			DocumentBuilderFactory dbF = DocumentBuilderFactory.newInstance();
			DocumentBuilder documentBuilder = dbF.newDocumentBuilder();
			Document doc = documentBuilder.parse(inputFile);
			doc.getDocumentElement().normalize();
			NodeList nodeList = doc.getElementsByTagName("server");
			
			for(int temp = 0; temp < nodeList.getLength(); temp++)
			{
				ServerInfo server = new ServerInfo();
				
				Node nNode = nodeList.item(temp);
				if(nNode.getNodeName().equals("server"))
				{
					if(nNode.getNodeType() == Node.ELEMENT_NODE)
					{
						Element eElement = (Element) nNode;
						
						//server type
						server.setServerType(eElement.getAttribute("type"));
						//server limit
						server.setLimit(Integer.parseInt(eElement.getAttribute("limit")));
						//bootupTime
						server.setBootupTime(Integer.parseInt(eElement.getAttribute("bootupTime")));
						//rate
						server.setRate(Float.parseFloat(eElement.getAttribute("rate")));
						//cpuCount
						server.setCpuCores(Integer.parseInt(eElement.getAttribute("coreCount")));
						//memory
						server.setMemory(Integer.parseInt(eElement.getAttribute("memory")));
						//disk
						server.setDisk(Integer.parseInt(eElement.getAttribute("disk")));		
						
						servers.add(server);
						
						if(!serverTypes.contains(server.getServerType()))
						{
							serverTypes.add(server.getServerType());
						}
					}					
				}				
			}			
		} 
		catch (Exception e) 
		{
			e.printStackTrace();
		}
	}		
}
