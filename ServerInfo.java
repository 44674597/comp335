
public class ServerInfo
{
	public String getServerType() {
		return serverType;
	}

	public void setServerType(String serverType) {
		this.serverType = serverType;
	}

	public int getiD() {
		return iD;
	}

	public void setiD(int iD) {
		this.iD = iD;
	}

	public int getServerState() {
		return serverState;
	}

	public void setServerState(int serverState) {
		this.serverState = serverState;
	}

	public int getAvailableTime() {
		return availableTime;
	}

	public void setAvailableTime(int availableTime) {
		this.availableTime = availableTime;
	}

	public int getCpuCores() {
		return cpuCores;
	}

	public void setCpuCores(int cpuCores) {
		this.cpuCores = cpuCores;
	}

	public int getMemory() {
		return memory;
	}

	public void setMemory(int memory) {
		this.memory = memory;
	}

	public int getDisk() {
		return disk;
	}

	public void setDisk(int disk) {
		this.disk = disk;
	}

	private String serverType;
	private int iD;
	private int serverState;
	private int availableTime;
	private int cpuCores;
	private int memory;
	private int disk;
	
	private int limit;
	private int bootupTime;
	private float rate;
	
	public int getLimit()
	{
		return limit;
	}
	
	public int getBootupTime()
	{
		return bootupTime;
	}
	
	public float getRate()
	{
		return rate;
	}
	
	public void setLimit(int val)
	{
		limit = val;
	}
	
	public void setBootupTime(int val)
	{
		bootupTime = val;
	}
	
	public void setRate(float val)
	{
		rate = val;
	}
	
	public ServerInfo()
	{
		
	}
}


